
#ifndef COMPILERLAB2_TOKENS_H
#define COMPILERLAB2_TOKENS_H

#include <string>
#include <vector>

extern int yylex(void);
extern FILE *yyin;

struct Token {
    std::string type;
    std::string value;
};

#endif //COMPILERLAB2_TOKENS_H
