#include <iostream>
#include "Tokens.h"
#include <unordered_map>
#include <queue>
#include <iterator>
#include <map>
#include <stack>

using namespace std;

class AddressMap {
public:
    // 添加新的变量名、地址和值映射关系
    void addMapping(const std::string& varName, int address, int value) {
        variableToAddress[varName] = address;
        addressToVariable[address] = varName;
        addressToValue[address] = value;
    }

    // 根据变量名查找地址
    int getAddress(const std::string& varName) const {
        auto it = variableToAddress.find(varName);
        if (it != variableToAddress.end()) {
            return it->second;
        } else {
            // 如果找不到变量名，返回一个特殊值，比如-1
            return -1;
        }
    }

    // 根据地址查找变量名
    std::string getVariableName(int address) const {
        auto it = addressToVariable.find(address);
        if (it != addressToVariable.end()) {
            return it->second;
        } else {
            return ""; // 或者其他你认为合适的默认值
        }
    }

    // 根据地址查找数值
    int getValue(int address) const {
        auto it = addressToValue.find(address);
        if (it != addressToValue.end()) {
            return it->second;
        } else {
            // 如果找不到地址，返回一个特殊值，比如-1
            return -1;
        }
    }

    // 根据地址修改数值
    void setValue(int address, int value) {
        auto it = addressToValue.find(address);
        if (it != addressToValue.end()) {
            it->second = value;
        } else {
            // 如果找不到地址，可以选择抛出异常或者忽略该操作
            std::cerr << "Address not found." << std::endl;
        }
    }

private:
    std::unordered_map<std::string, int> variableToAddress;
    std::unordered_map<int, std::string> addressToVariable;
    std::unordered_map<int, int> addressToValue;
};

extern vector<Token> tokens;

// 定义运算符的优先级
std::map<std::string, int> precedence = {
        {"|", 1},
        {"^", 2},
        {"&", 3},
        {"==", 4}, {"!=", 4},
        {">", 5}, {">=", 5}, {"<", 5}, {"<=", 5},
        {"+", 6}, {"-", 6},
        {"*", 7}, {"/", 7}, {"%", 7}
};

bool isOperator(const std::string& token) {
    return precedence.find(token) != precedence.end();
}

bool isHigherPrecedence(const std::string& op1, const std::string& op2) {
    return precedence[op1] >= precedence[op2];
}

int evaluateOperation(int operand1, int operand2, const std::string& op) {
    if (op == "+") return operand1 + operand2;
    if (op == "-") return operand1 - operand2;
    if (op == "*") return operand1 * operand2;
    if (op == "/") return operand1 / operand2;
    if (op == "%") return operand1 % operand2;
    if (op == "<") return operand1 < operand2;
    if (op == "<=") return operand1 <= operand2;
    if (op == ">") return operand1 > operand2;
    if (op == ">=") return operand1 >= operand2;
    if (op == "==") return operand1 == operand2;
    if (op == "!=") return operand1 != operand2;
    if (op == "&") return operand1 & operand2;
    if (op == "|") return operand1 | operand2;
    if (op == "^") return operand1 ^ operand2;
    return 0; // Error
}

std::queue<Token> infixToPostfix(std::queue<Token>& infix) {
    std::queue<Token> postfix;
    std::stack<Token> operatorStack;

    while (!infix.empty()) {
        Token token = infix.front();
        infix.pop();

        if (token.type == "Integer" || token.type == "Identifier") {
//            if (token.type == "Integer"){
//                cout<<"mov eax,"<<token.value<<endl;
//            }else{
//                cout<<"mov eax, DWORD PTR [ebp-"<<addressMap.getAddress(token.value)<<"]";
//            }
            postfix.push(token);
        } else if (isOperator(token.value)) {
            while (!operatorStack.empty() && operatorStack.top().type == "Operator" && isHigherPrecedence(operatorStack.top().value, token.value)) {
                postfix.push(operatorStack.top());
                operatorStack.pop();
            }
            operatorStack.push(token);
        } else if (token.value == "(") {
            operatorStack.push(token);
        } else if (token.value == ")") {
            while (!operatorStack.empty() && operatorStack.top().value != "(") {
                postfix.push(operatorStack.top());
                operatorStack.pop();
            }
            operatorStack.pop();
        }
    }

    while (!operatorStack.empty()) {
        postfix.push(operatorStack.top());
        operatorStack.pop();
    }

    return postfix;
}

int evaluatePostfixExpression(std::queue<Token>& postfix, const AddressMap& addressMap) {
    std::stack<int> operandStack;

    while (!postfix.empty()) {
        Token token = postfix.front();
        postfix.pop();

        if (token.type == "Integer") {
            cout<<"mov eax,"<<token.value<<endl;
            cout<<"push eax"<<endl;
            operandStack.push(std::stoi(token.value));
        } else if (token.type == "Identifier") {
            int address = addressMap.getAddress(token.value);
            cout<<"mov eax, DWORD PTR [ebp-"<<address<<"]\n";
            cout<<"push eax"<<endl;
            operandStack.push(addressMap.getValue(address));
        } else if (isOperator(token.value)) {
            int operand2 = operandStack.top();
            operandStack.pop();
            cout<<"pop ebx\n";
            int operand1 = operandStack.top();
            cout<<"pop eax\n";
            operandStack.pop();

            if (token.value == "+") {
                cout << "add eax, ebx" << endl;
            } else if (token.value == "-") {
                cout << "sub eax, ebx" << endl;
            } else if (token.value == "*") {
                cout << "imul eax, ebx" << endl;
            } else if (token.value == "/") {
                cout << "cdq" << endl;
                cout << "idiv ebx" << endl;
            } else if (token.value == "%") {
                cout << "cdq" << endl;
                cout << "idiv ebx" << endl;
                cout << "mov eax, edx" << endl;
            } else if (token.value == "<") {
                cout << "cmp eax, ebx" << endl;
                cout << "setl al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == "<=") {
                cout << "cmp eax, ebx" << endl;
                cout << "setle al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == ">") {
                cout << "cmp eax, ebx" << endl;
                cout << "setg al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == ">=") {
                cout << "cmp eax, ebx" << endl;
                cout << "setge al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == "==") {
                cout << "cmp eax, ebx" << endl;
                cout << "sete al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == "!=") {
                cout << "cmp eax, ebx" << endl;
                cout << "setne al" << endl;
                cout << "movzx eax, al" << endl;
            } else if (token.value == "&") {
                cout << "and eax, ebx" << endl;
            } else if (token.value == "|") {
                cout << "or eax, ebx" << endl;
            } else if (token.value == "^") {
                cout << "xor eax, ebx" << endl;
            } else {
                cout << "Unsupported operation" << endl;
            }

            cout<<"push eax\n";

            int result = evaluateOperation(operand1, operand2, token.value);
            operandStack.push(result);
        }
    }
    cout<<"pop eax\n";
    return operandStack.top();
}

int main(int argc, char* argv[]) {
    if (argc != 2) {
        return 1;
    }

    FILE* input_file = fopen(argv[1], "r");
    if (!input_file) {
        return 1;
    }

    yyin = input_file;
    yylex();
    fclose(input_file);

    int state = 0;
    string tempVariable;
    int newAddress = 4;
    auto it = tokens.begin();
    AddressMap addressMap;
    queue<Token> infixExpression;

    cout<<".intel_syntax noprefix\n";
    cout<<".global main\n";
    cout<<".extern printf\n";
    cout<<".data\n";
    cout<<"format_str:\n";
    cout<<".asciz \"%d\\n\"\n";
    cout<<".text\n";
    cout<<"main:\n";
    cout<<"push ebp\n";
    cout<<"mov ebp, esp\n";
    cout<<"sub esp, 0x100\n";


    while(true){
//        cout<<state<<" "<<tempVariable<<" "<<newAddress<<endl;
        switch(state){
            case 0:
                if (it->type=="KeyWord"){
                    if(it->value=="int"){
                        state = 1;
                        advance(it, 1);
                        break;
                    }
                    else if(it->value=="return"){
                        state = 14;
                        advance(it, 1);
                        break;
                    }
                }
                else if (it->type=="Identifier"){
                    state = 6;
                    tempVariable = it->value;
                    advance(it, 1);
                    break;
                }
                else if (it->type=="FunctionCall"){
                    state = 9;
                    advance(it, 1);
                    break;
                }
                else if(it->type=="Punctuation"&&it->value=="}"){
                    state = 17;
                    break;
                }

            case 1:
                if (it->type=="Identifier"){
                    state = 2;
                    tempVariable = it->value;
                    advance(it, 1);
                    break;
                }
                else if (it->type=="KeyWord"&&it->value=="main"){
                    state = 4;
                    advance(it, 1);
                    break;
                }

            case 2:
                if (it->type=="Punctuation"&&it->value==";"){
                    state = 3;
                    advance(it, 1);
                    break;
                }

            case 3:
                //终止状态
                state = 0;
                addressMap.addMapping(tempVariable, newAddress, 0);
                cout<<"mov DWORD PTR [ebp-"<<newAddress<<"], 0\n";
                newAddress += 4;
                break;

            case 4:
                if (it->type=="Punctuation"&&it->value=="{"){
                    state = 5;
                    advance(it, 1);
                    break;
                }
                else{
                    state = 4;
                    advance(it, 1);
                    break;
                }

            case 5:
                //终止状态
                state = 0;
                break;

            case 6:
                if(it->type=="Operator"&&it->value=="="){
                    state = 7;
                    advance(it, 1);
                    break;
                }

            case 7:
                if (it->type=="Punctuation"&&it->value==";"){
                    queue<Token> postfixExpression = infixToPostfix(infixExpression);

                    int result = evaluatePostfixExpression(postfixExpression, addressMap);
                    cout<<"mov DWORD PTR [ebp-"<<addressMap.getAddress(tempVariable)<<"], eax\n";

                    addressMap.setValue(addressMap.getAddress(tempVariable),result);
//                    cout<<tempVariable <<" = "<<result<<endl;

                    while (!infixExpression.empty()){
                        infixExpression.pop();
                    }

                    state = 8;

                    advance(it, 1);
                    break;
                }
                else{
                    infixExpression.push(*it);
                    state = 7;
                    advance(it, 1);
                    break;
                }

            case 8:
                //终止状态
                while (!infixExpression.empty()){
                    infixExpression.pop();
                }
                state = 0;
                break;

            case 9:
                if(it->type=="Punctuation"&&it->value=="("){
                    state = 10;
                    advance(it, 1);
                    break;
                }

            case 10:
                if(it->type=="Identifier"){
                    tempVariable = it->value;
                    cout<<"push DWORD PTR [ebp-"<<addressMap.getAddress(tempVariable)<<"]\n";
                    cout<<"push offset format_str\n";
                    cout<<"call printf\n";
                    cout<<"add esp, 8\n";
                    state = 11;
                    advance(it, 1);
                    break;
                }
                else if(it->type=="Integer"){
//                    cout<<"printFunction: "<<it->value<<endl;
                    state = 11;
                    advance(it, 1);
                    break;
                }

            case 11:
                if(it->type=="Punctuation"&&it->value==")"){
                    state = 12;
                    advance(it, 1);
                    break;
                }

            case 12:
                if(it->type=="Punctuation"&&it->value==";"){
                    state = 13;
                    advance(it, 1);
                    break;
                }

            case 13:
                //终结状态
                state = 0;
                break;

            case 14:
                if(it->type=="Identifier"){
                    tempVariable = it->value;
                    cout<<"mov eax, DWORD PTR [ebp-"<<addressMap.getAddress(tempVariable)<<"]\n";
                    state = 15;
                    advance(it, 1);
                    break;
                }
                else if(it->type=="Integer"){
                    cout<<"mov eax, "<<it->value<<"\n";
                    state = 15;
                    advance(it, 1);
                    break;
                }

            case 15:
                if(it->type=="Punctuation"&&it->value==";"){
                    state = 16;
                    advance(it, 1);
                    break;
                }

            case 16:
                //终结状态
                state = 0;
                break;
        }

        if (state==17){
            //程序结束
            cout<<"leave\nret\n";
            break;
        }
    }

    return 0;
}
